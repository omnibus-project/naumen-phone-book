package ru.jacobs.naumen.presenter;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import net.customware.gwt.dispatch.client.DispatchAsync;
import ru.jacobs.naumen.event.AddUserEvent;
import ru.jacobs.naumen.event.EditUserEvent;
import ru.jacobs.naumen.shared.User;
import ru.jacobs.naumen.shared.dispatch.DeleteUserAction;
import ru.jacobs.naumen.shared.dispatch.DeleteUserResult;
import ru.jacobs.naumen.shared.dispatch.GetUserListAction;
import ru.jacobs.naumen.shared.dispatch.GetUserListResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jacob on 24.03.2016.
 */
public class UsersPresenter implements Presenter {

    private List<User> users;

    public interface Display {
        HasClickHandlers getAddButton();
        HasClickHandlers getSearchButton();
        HasClickHandlers getEditDialogBoxButton();
        HasClickHandlers getDeleteDialogBoxButton();
        HasClickHandlers getCloseDialogBoxButton();
        HasClickHandlers getCloseDeleteDialogBoxButton();
        HasClickHandlers getGoodDeleteDialogBoxButton();
        void showDialogError(String errMsg);
        void showDialogDelete(String msg);
        Column getDeleteButton();
        Column getEditButton();
        Column getNameColumn();
        Column getPhoneColumn();
        String getSearchText();
        void setData(List<User> data);
        void hideDialogBox();
        Widget asWidget();
        DialogBox setDialogBox(long id, String name, String phone);
        User getClickedRow();
    }

    private final DispatchAsync dispatch;
    private final HandlerManager eventBus;
    private final Display display;

    public UsersPresenter(DispatchAsync dispatch, HandlerManager eventBus, Display view) {
        this.dispatch = dispatch;
        this.eventBus = eventBus;
        this.display = view;
    }

    public void bind() {
        display.getAddButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new AddUserEvent());
            }
        });
        display.getSearchButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                search(display.getSearchText());
            }
        });
        display.getEditButton().setFieldUpdater( new FieldUpdater<User, String>() {
            @Override
            public void update(int index, User object, String value) {
                eventBus.fireEvent(new EditUserEvent(object.getId()));
            }
        });
        display.getEditButton().setFieldUpdater( new FieldUpdater<User, String>() {
            @Override
            public void update(int index, User object, String value) {
                eventBus.fireEvent(new EditUserEvent(object.getId()));
            }
        });
        display.getNameColumn().setFieldUpdater( new FieldUpdater<User, String>() {
            @Override
            public void update(int index, User object, String value) {
                setDialogBox(object);
            }
        });
        display.getPhoneColumn().setFieldUpdater( new FieldUpdater<User, String>() {
            @Override
            public void update(int index, User object, String value) {
                setDialogBox(object);
            }
        });
        display.getDeleteButton().setFieldUpdater( new FieldUpdater<User, String>() {
            @Override
            public void update(int index, User object, String value) {
                deleteUser(object);
            }
        });
    }

    @Override
    public void go(HasWidgets container) {
        bind();
        container.clear();
        container.add(display.asWidget());
        fetchContactDetails();
    }

    private void setDialogBox(User object) {
        display.setDialogBox(object.getId(), object.getName(), object.getPhone());
        display.getCloseDialogBoxButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideDialogBox();
            }
        });
        display.getEditDialogBoxButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideDialogBox();
                eventBus.fireEvent(new EditUserEvent(display.getClickedRow().getId()));
            }
        });
        display.getDeleteDialogBoxButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideDialogBox();
                deleteUser(display.getClickedRow());
            }
        });
    }

    private void search(final String searchString) {
        dispatch.execute(new GetUserListAction(), new AsyncCallback<GetUserListResult>() {
            List<User> newContacts = new ArrayList<User>();

            String pattern = "^[0-9+ ()]+$";
            RegExp regExp = RegExp.compile(pattern);
            MatchResult matcher = regExp.exec(searchString);
            boolean matchFound = matcher != null;
            public void onSuccess(GetUserListResult result) {
                users = result.getUserList();
                if(matchFound) {
                    for (int i = 0; i < users.size(); i++) {
                        String name = users.get(i).getPhone();
                        if (name.indexOf(searchString) != -1) {
                            newContacts.add(users.get(i));
                        }
                    }
                } else {
                    for (int i = 0; i < users.size(); i++) {
                        String name = users.get(i).getName();
                        if (name.indexOf(searchString) != -1) {
                            newContacts.add(users.get(i));
                        }
                    }
                }
                if(newContacts.size() <= 0) {
                    showError("По вашему запросу ничего не нашлось");
//                выводим список всех контактов
                    display.setData(users);
                } else {
//                выводим отфильтрованный список
                    display.setData(newContacts);
                }
            }

            public void onFailure(Throwable caught) {
                showError("Ошибка выборки контактных данных");
            }
        });

    }

    private void showError(String errMsg) {
        this.display.showDialogError(errMsg);
        this.display.getCloseDialogBoxButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideDialogBox();
            }
        });
    }


    private void fetchContactDetails() {
        dispatch.execute(new GetUserListAction(), new AsyncCallback<GetUserListResult>() {
            @Override
            public void onFailure(Throwable throwable) {
                showError("Ошибка выборки контактных данных");
            }

            @Override
            public void onSuccess(GetUserListResult getUserListResult) {
                display.setData(getUserListResult.getUserList());
            }
        });
    }

    private void deleteUser(final User selected) {
        display.showDialogDelete("Вы уверены что хотите удалить контакт?");
        display.getCloseDeleteDialogBoxButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideDialogBox();
            }
        });
        display.getGoodDeleteDialogBoxButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                dispatch.execute(new DeleteUserAction(selected.getId()), new AsyncCallback<DeleteUserResult>() {

                    @Override
                    public void onSuccess(DeleteUserResult result) {
                        if(result.getException() != null) {
                            Window.alert(result.getException().getMessage());
                        } else {
                            fetchContactDetails();
                        }
                    }

                    @Override
                    public void onFailure(Throwable caught) {
                        showError("Ошибка удаления контакта");
                    }
                });
                display.hideDialogBox();
            }
        });
    }
}
