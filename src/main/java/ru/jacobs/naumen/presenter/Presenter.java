package ru.jacobs.naumen.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * Created by Jacob on 24.03.2016.
 */
public abstract interface Presenter {
    public abstract void go(final HasWidgets container);
}
