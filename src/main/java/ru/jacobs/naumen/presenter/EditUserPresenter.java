package ru.jacobs.naumen.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import net.customware.gwt.dispatch.client.DispatchAsync;
import ru.jacobs.naumen.event.EditUserCancelledEvent;
import ru.jacobs.naumen.event.UpdateUserEvent;
import ru.jacobs.naumen.shared.User;
import ru.jacobs.naumen.shared.dispatch.*;

/**
 * Created by Jacob on 24.03.2016.
 */
public class EditUserPresenter implements Presenter {

    public interface Display {
        HasClickHandlers getSaveButton();
        HasClickHandlers getCancelButton();
        HasClickHandlers getCloseDialogBoxButton();
        HasValue<String> getName();
        HasValue<String> getPhone();
        void showDialogError(String errMsg);
        void hideDialogError();
        Widget asWidget();
    }
    private User user;
    private String type;
    private final DispatchAsync dispatch;
    private final HandlerManager eventBus;
    private final Display display;

    public EditUserPresenter(DispatchAsync dispatch, HandlerManager eventBus, Display display) {
        this.dispatch = dispatch;
        this.eventBus = eventBus;
        this.user = new User();
        this.display = display;
        this.type = "add";
        bind();
    }

    public EditUserPresenter(DispatchAsync dispatch, HandlerManager eventBus, Display display, long id) {
        this.dispatch = dispatch;
        this.eventBus = eventBus;
        this.display = display;
        this.type = "update";
        bind();

        dispatch.execute(new GetUserAction(id), new AsyncCallback<GetUserResult>() {
            public void onSuccess(GetUserResult result) {
                user = result.getUser();
                EditUserPresenter.this.display.getName().setValue(user.getName());
                EditUserPresenter.this.display.getPhone().setValue(user.getPhone());
            }

            public void onFailure(Throwable caught) {
                showError("Ошибка при получении контакта");
            }
        });
    }

    public void bind() {
        this.display.getSaveButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                doSave();
            }
        });

        this.display.getCancelButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new EditUserCancelledEvent());
            }
        });
    }

    @Override
    public void go(HasWidgets container) {
        container.clear();
        container.add(display.asWidget());
    }

    private void showError(String errMsg) {
        this.display.showDialogError(errMsg);
        this.display.getCloseDialogBoxButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideDialogError();
            }
        });
    }

    private void doSave() {
            user.setName(display.getName().getValue());
            user.setPhone(display.getPhone().getValue());

           dispatch.execute(new TestDuplicateUserAction(user), new AsyncCallback<TestDuplicateUserResult>() {
                @Override
                public void onSuccess(TestDuplicateUserResult result) {
                    if(result.getException() != null){
                        showError("Ошибка добавления контакта!!!");
                    } else {
                        if(result.getAnswer()) {
                            if(type == "update") {
                                dispatch.execute(new UpdateUserAction(user), new AsyncCallback<UpdateUserResult>() {
                                    public void onSuccess(UpdateUserResult result) {
                                        if(result.getException() != null) {
                                            Window.alert(result.getException().getMessage());
                                        } else {
                                            eventBus.fireEvent(new UpdateUserEvent(result.getUser()));
                                        }
                                    }
                                    public void onFailure(Throwable caught) {
                                        showError("Ошибка добавления контакта");
                                    }
                                });
                            } else {
                                dispatch.execute(new AddUserAction(user), new AsyncCallback<AddUserResult>() {
                                    public void onSuccess(AddUserResult result) {
                                        if(result.getException() != null) {
                                            Window.alert(result.getException().getMessage());
                                        } else {
                                            eventBus.fireEvent(new UpdateUserEvent(result.getUser()));
                                        }
                                    }
                                    public void onFailure(Throwable caught) {
                                        showError("Ошибка добавления контакта!!");
                                    }
                                });
                            }
                        } else {
                            showError("Ошибка добавления контакта!");
                        }
                    }
                }

                @Override
                public void onFailure(Throwable caught) {
                    showError("Ошибка добавления контакта# "+caught.getMessage());
                }
            });

    }

}
