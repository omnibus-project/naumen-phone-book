package ru.jacobs.naumen.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import ru.jacobs.naumen.event.EditUserCancelledEvent;

/**
 * Created by Jacob on 28.03.2016.
 */
public class NotFoundPresenter implements Presenter {
    public interface Display {
        HasClickHandlers getCancelClickHandlers();
        Widget asWidget();
    }

    private final HandlerManager eventBus;
    private final Display display;

    public NotFoundPresenter(HandlerManager eventBus, Display display) {
        this.eventBus = eventBus;
        this.display = display;
    }

    private void bind() {
        display.getCancelClickHandlers().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(new EditUserCancelledEvent());
            }
        });
    }

    @Override
    public void go(HasWidgets container) {
        bind();
        container.clear();
        container.add(display.asWidget());
    }

}
