package ru.jacobs.naumen.shared.dispatch;

import net.customware.gwt.dispatch.shared.Action;
import ru.jacobs.naumen.shared.User;

/**
 * Created by Jacob on 28.03.2016.
 */
public class AddUserAction implements Action<AddUserResult> {
    private User user;
    AddUserAction() {
    }
    public AddUserAction(User user) {
        this.user = user;
    }
    public User getUser() {
        return this.user;
    }
}
