package ru.jacobs.naumen.shared.dispatch;

import net.customware.gwt.dispatch.shared.Action;

/**
 * Created by Jacob on 28.03.2016.
 */
public class GetUserAction implements Action<GetUserResult> {
    private long id;
    GetUserAction() {
    }
    public GetUserAction(long id) {
        this.id = id;
    }
    public long getId() {
        return this.id;
    }
}
