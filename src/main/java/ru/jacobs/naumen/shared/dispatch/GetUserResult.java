package ru.jacobs.naumen.shared.dispatch;

import net.customware.gwt.dispatch.shared.Result;
import ru.jacobs.naumen.shared.User;

/**
 * Created by Jacob on 28.03.2016.
 */
public class GetUserResult implements Result {
    private User user;
    private Exception exception;
    GetUserResult() {
    }
    public GetUserResult(User user) {
        this.user = user;
    }
    public GetUserResult(Exception exception) {
        this.exception = exception;
    }
    public User getUser() {
        return this.user;
    }
    public Exception getException(){
        return this.exception;
    }
}
