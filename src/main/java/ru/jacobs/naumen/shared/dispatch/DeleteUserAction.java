package ru.jacobs.naumen.shared.dispatch;

import net.customware.gwt.dispatch.shared.Action;
import ru.jacobs.naumen.shared.User;

/**
 * Created by Jacob on 28.03.2016.
 */
public class DeleteUserAction implements Action<DeleteUserResult> {
    private long id;
    DeleteUserAction() {
    }
    public DeleteUserAction(long id) {
        this.id = id;
    }
    public long getId() {
        return this.id;
    }
}
