package ru.jacobs.naumen.shared.dispatch;

import net.customware.gwt.dispatch.shared.Action;
import ru.jacobs.naumen.shared.User;

/**
 * Created by Jacob on 28.03.2016.
 */
public class UpdateUserAction implements Action<UpdateUserResult> {
    private User user;
    UpdateUserAction() {
    }
    public UpdateUserAction(User user) {
        this.user = user;
    }
    public User getUser() {
        return this.user;
    }
}
