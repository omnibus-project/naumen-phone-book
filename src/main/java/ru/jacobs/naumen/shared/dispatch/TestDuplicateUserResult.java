package ru.jacobs.naumen.shared.dispatch;

import net.customware.gwt.dispatch.shared.Result;
import ru.jacobs.naumen.shared.User;

/**
 * Created by Jacob on 28.03.2016.
 */
public class TestDuplicateUserResult implements Result {
    private Exception exception;
    private Boolean answer;
    public TestDuplicateUserResult() {
    }
    public TestDuplicateUserResult(Boolean answer) {
        this.answer = answer;
    }
    public Boolean getAnswer() {
        return this.answer;
    }
    public TestDuplicateUserResult(Exception exception) {
        this.exception = exception;
    }
    public Exception getException(){
        return this.exception;
    }
}
