package ru.jacobs.naumen.shared.dispatch;

import net.customware.gwt.dispatch.shared.Result;
import ru.jacobs.naumen.shared.User;

import java.util.ArrayList;

/**
 * Created by Jacob on 28.03.2016.
 */
public class GetUserListResult implements Result {
    private Exception exception;
    private ArrayList<User> userList;
    GetUserListResult(){
    }
    public GetUserListResult(ArrayList<User> userList) {
        this.userList = userList;
    }
    public GetUserListResult(Exception exception) {
        this.exception = exception;
    }
    public ArrayList<User> getUserList() {
        return this.userList;
    }
    public Exception getException(){
        return this.exception;
    }
}
