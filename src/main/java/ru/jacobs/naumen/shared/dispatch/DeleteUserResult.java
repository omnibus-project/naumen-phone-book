package ru.jacobs.naumen.shared.dispatch;

import net.customware.gwt.dispatch.shared.Result;

/**
 * Created by Jacob on 28.03.2016.
 */
public class DeleteUserResult implements Result {
    private Exception exception;
    private Boolean answer;
    public DeleteUserResult() {
    }
    public DeleteUserResult(Boolean answer) {
        this.answer = answer;
    }
    public DeleteUserResult(Exception exception) {
        this.exception = exception;
    }
    public Exception getException(){
        return this.exception;
    }
}
