package ru.jacobs.naumen.shared.dispatch;

import net.customware.gwt.dispatch.shared.Result;
import ru.jacobs.naumen.shared.User;

/**
 * Created by Jacob on 28.03.2016.
 */
public class UpdateUserResult implements Result {
    private Exception exception;
    private User user;
    public UpdateUserResult() {
    }
    public UpdateUserResult(User user) {
        this.user = user;
    }
    public User getUser() {
        return this.user;
    }
    public UpdateUserResult(Exception exception) {
        this.exception = exception;
    }
    public Exception getException(){
        return this.exception;
    }
}
