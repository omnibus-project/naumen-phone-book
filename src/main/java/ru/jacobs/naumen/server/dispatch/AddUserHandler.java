package ru.jacobs.naumen.server.dispatch;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.ActionException;
import net.customware.gwt.dispatch.shared.DispatchException;
import ru.jacobs.naumen.server.model.UserDAO;
import ru.jacobs.naumen.server.model.UserDAOImpl;
import ru.jacobs.naumen.server.model.UserDAOSingleton;
import ru.jacobs.naumen.shared.dispatch.AddUserAction;
import ru.jacobs.naumen.shared.dispatch.AddUserResult;

/**
 * Created by Jacob on 28.03.2016.
 */
public class AddUserHandler implements ActionHandler<AddUserAction, AddUserResult> {
    @Override
    public Class<AddUserAction> getActionType() {
        return AddUserAction.class;
    }

    @Override
    public AddUserResult execute(AddUserAction addUserAction, ExecutionContext executionContext) throws ActionException {
        try {
            UserDAO userDAO = UserDAOSingleton.getInstance();
            return new AddUserResult(userDAO.addUsers(addUserAction.getUser()));
        }
        catch (Exception e) {
            return new AddUserResult(e);
        }
    }

    @Override
    public void rollback(AddUserAction addUserAction, AddUserResult addUserResult, ExecutionContext executionContext) throws ActionException {

    }
}
