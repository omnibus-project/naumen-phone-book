package ru.jacobs.naumen.server.dispatch;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.ActionException;
import net.customware.gwt.dispatch.shared.DispatchException;
import ru.jacobs.naumen.server.model.UserDAO;
import ru.jacobs.naumen.server.model.UserDAOSingleton;
import ru.jacobs.naumen.shared.dispatch.TestDuplicateUserAction;
import ru.jacobs.naumen.shared.dispatch.TestDuplicateUserResult;

/**
 * Created by Jacob on 28.03.2016.
 */
public class TestDuplicateUserHandler implements ActionHandler<TestDuplicateUserAction, TestDuplicateUserResult> {
    @Override
    public Class<TestDuplicateUserAction> getActionType() {
        return TestDuplicateUserAction.class;
    }

    @Override
    public TestDuplicateUserResult execute(TestDuplicateUserAction testDuplicateUserAction, ExecutionContext executionContext) throws ActionException {
        try {
            UserDAO userDAO = UserDAOSingleton.getInstance();
            return new TestDuplicateUserResult(userDAO.testDuplicateUser(testDuplicateUserAction.getUser()));
        }
        catch (Exception e) {
            return new TestDuplicateUserResult(e);
        }
    }

    @Override
    public void rollback(TestDuplicateUserAction testDuplicateUserAction, TestDuplicateUserResult testDuplicateUserResult, ExecutionContext executionContext) throws ActionException {

    }
}
