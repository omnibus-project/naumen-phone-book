package ru.jacobs.naumen.server.dispatch;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import net.customware.gwt.dispatch.client.standard.StandardDispatchService;
import net.customware.gwt.dispatch.server.DefaultActionHandlerRegistry;
import net.customware.gwt.dispatch.server.Dispatch;
import net.customware.gwt.dispatch.server.InstanceActionHandlerRegistry;
import net.customware.gwt.dispatch.server.SimpleDispatch;
import net.customware.gwt.dispatch.shared.Action;
import net.customware.gwt.dispatch.shared.DispatchException;
import net.customware.gwt.dispatch.shared.Result;

/**
 * Created by Jacob on 28.03.2016.
 */
public class DispatchServlet extends RemoteServiceServlet implements StandardDispatchService {
    private final Dispatch dispatch;

    public DispatchServlet() {

        InstanceActionHandlerRegistry registry = new DefaultActionHandlerRegistry();
// Регистрируем обработчики
        registry.addHandler(new GetUserListHandler());
        registry.addHandler(new GetUserHandler());
        registry.addHandler(new DeleteUserHandler());
        registry.addHandler(new AddUserHandler());
        registry.addHandler(new TestDuplicateUserHandler());
        registry.addHandler(new UpdateUserHandler());
        dispatch = new SimpleDispatch(registry);
    }

    @Override
    public Result execute(Action<?> action) throws DispatchException {
        return dispatch.execute(action);
    }
}
