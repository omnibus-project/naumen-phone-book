package ru.jacobs.naumen.server.dispatch;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.ActionException;
import net.customware.gwt.dispatch.shared.DispatchException;
import ru.jacobs.naumen.server.model.UserDAO;
import ru.jacobs.naumen.server.model.UserDAOSingleton;
import ru.jacobs.naumen.shared.dispatch.UpdateUserAction;
import ru.jacobs.naumen.shared.dispatch.UpdateUserResult;

/**
 * Created by Jacob on 28.03.2016.
 */
public class UpdateUserHandler implements ActionHandler<UpdateUserAction, UpdateUserResult> {
    @Override
    public Class<UpdateUserAction> getActionType() {
        return UpdateUserAction.class;
    }

    @Override
    public UpdateUserResult execute(UpdateUserAction updateUserAction, ExecutionContext executionContext) throws ActionException {
        try {
            UserDAO userDAO = UserDAOSingleton.getInstance();
            return new UpdateUserResult(userDAO.updateUsers(updateUserAction.getUser()));
        }
        catch (Exception e) {
            return new UpdateUserResult(e);
        }
    }

    @Override
    public void rollback(UpdateUserAction updateUserAction, UpdateUserResult updateUserResult, ExecutionContext executionContext) throws ActionException {

    }
}
