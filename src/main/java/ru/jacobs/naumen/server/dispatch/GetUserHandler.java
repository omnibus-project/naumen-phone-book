package ru.jacobs.naumen.server.dispatch;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.ActionException;
import net.customware.gwt.dispatch.shared.DispatchException;
import ru.jacobs.naumen.server.model.UserDAO;
import ru.jacobs.naumen.server.model.UserDAOImpl;
import ru.jacobs.naumen.server.model.UserDAOSingleton;
import ru.jacobs.naumen.shared.dispatch.GetUserAction;
import ru.jacobs.naumen.shared.dispatch.GetUserResult;


/**
 * Created by Jacob on 28.03.2016.
 */
public class GetUserHandler implements ActionHandler<GetUserAction, GetUserResult> {

    @Override
    public Class<GetUserAction> getActionType() {
        return GetUserAction.class;
    }

    @Override
    public GetUserResult execute(GetUserAction getUserAction, ExecutionContext executionContext) throws ActionException {
        try {
            UserDAO userDAO = UserDAOSingleton.getInstance();
            return new GetUserResult(userDAO.getUsers(getUserAction.getId()));
        }
        catch (Exception e) {
            return new GetUserResult(e);
        }
    }

    @Override
    public void rollback(GetUserAction getUserAction, GetUserResult getUserResult, ExecutionContext executionContext) throws ActionException {

    }
}
