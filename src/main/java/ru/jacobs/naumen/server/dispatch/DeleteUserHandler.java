package ru.jacobs.naumen.server.dispatch;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.ActionException;
import net.customware.gwt.dispatch.shared.DispatchException;
import ru.jacobs.naumen.server.model.UserDAO;
import ru.jacobs.naumen.server.model.UserDAOImpl;
import ru.jacobs.naumen.server.model.UserDAOSingleton;
import ru.jacobs.naumen.shared.User;
import ru.jacobs.naumen.shared.dispatch.DeleteUserAction;
import ru.jacobs.naumen.shared.dispatch.DeleteUserResult;
import ru.jacobs.naumen.shared.dispatch.GetUserListAction;
import ru.jacobs.naumen.shared.dispatch.GetUserListResult;

import java.util.ArrayList;

/**
 * Created by Jacob on 28.03.2016.
 */
public class DeleteUserHandler implements ActionHandler<DeleteUserAction, DeleteUserResult> {
    @Override
    public Class<DeleteUserAction> getActionType() {
        return DeleteUserAction.class;
    }

    @Override
    public DeleteUserResult execute(DeleteUserAction deleteUserAction, ExecutionContext executionContext) throws ActionException {
        try {
            UserDAO userDAO = UserDAOSingleton.getInstance();
            userDAO.deleteUser(deleteUserAction.getId());
            return new DeleteUserResult(true);
        }
        catch (Exception e) {
            return new DeleteUserResult(e);
        }
    }

    @Override
    public void rollback(DeleteUserAction deleteUserAction, DeleteUserResult deleteUserResult, ExecutionContext executionContext) throws ActionException {

    }
}
