package ru.jacobs.naumen.server.dispatch;

import net.customware.gwt.dispatch.server.ActionHandler;
import net.customware.gwt.dispatch.server.ExecutionContext;
import net.customware.gwt.dispatch.shared.ActionException;
import net.customware.gwt.dispatch.shared.DispatchException;
import ru.jacobs.naumen.server.model.UserDAO;
import ru.jacobs.naumen.server.model.UserDAOImpl;
import ru.jacobs.naumen.server.model.UserDAOSingleton;
import ru.jacobs.naumen.shared.dispatch.GetUserListAction;
import ru.jacobs.naumen.shared.dispatch.GetUserListResult;

/**
 * Created by Jacob on 28.03.2016.
 */
public class GetUserListHandler implements ActionHandler<GetUserListAction, GetUserListResult> {
    @Override
    public Class<GetUserListAction> getActionType() {
        return GetUserListAction.class;
    }

    @Override
    public GetUserListResult execute(GetUserListAction getUserListAction, ExecutionContext executionContext) throws ActionException {
        try {
            UserDAO userDAO = UserDAOSingleton.getInstance();
            return new GetUserListResult(userDAO.getUsersDetails());
        }
        catch (Exception e) {
            return new GetUserListResult(e);
        }
    }

    @Override
    public void rollback(GetUserListAction getUserListAction, GetUserListResult getUserListResult, ExecutionContext executionContext) throws ActionException  {

    }
}
