package ru.jacobs.naumen.server.model;

/**
 * Created by Jacob on 28.03.2016.
 */
public class UserDAOSingleton {
    private static UserDAO instance;

    public static synchronized UserDAO getInstance() {
        if (instance == null) {
            instance = new UserDAOImpl();
        }

        return instance;
    }
}
