package ru.jacobs.naumen.server.model;

import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import org.eclipse.jetty.server.Authentication;
import ru.jacobs.naumen.shared.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Jacob on 28.03.2016.
 */
public class UserDAOImpl implements UserDAO {
    private final HashMap<Long, User> users = new HashMap<Long, User>();

    private static final String[] usersNameData = new String[] {
            "Андрей",
            "Александр",
            "Мария",
            "Ольга",
            "James",
            "Nicole",
            "Евгений"};

    private final String[] usersPhoneData = new String[] {
            "+791123423212",
            "+792342327112",
            "+791158797887",
            "+791234234232",
            "+792222334232",
            "+792234234232",
            "+792131321311"};

    public UserDAOImpl() {
        initUsers();
    }
    private void initUsers() {
        for (int i = 0; i < usersNameData.length && i < usersPhoneData.length; ++i) {
            User user = new User((long)i, usersNameData[i], usersPhoneData[i]);
            addUsers(user);
        }
    }

    public Boolean validUser(User userTest) {
        String patternPhone = "^[0-9 ()+]+$";
        String patternName = "^[\\pL ]+$";
        RegExp regExpName = RegExp.compile(patternName);
        RegExp regExpPhone = RegExp.compile(patternPhone);
        MatchResult matcherName = regExpName.exec(userTest.getName());
        MatchResult matcherPhone = regExpPhone.exec(userTest.getPhone());
        boolean matchFoundName = matcherName != null;
        boolean matchFoundPhone = matcherPhone != null;
        if(matchFoundName && matchFoundPhone) {
            ArrayList<User> users = getUsersDetails();
            for (User user : users) {
                if (user.getName().equals(userTest.getName()) && user.getPhone().equals(userTest.getPhone())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public User addUsers(User user) {
        if(validUser(user)) {
            user.setId((long) users.size());
            users.put(user.getId(), user);
        }
        return user;
    }

    public Boolean deleteUser(Long id) {
        users.remove(id);
        return true;
    }

    @Override
    public Boolean testDuplicateUser(User user) {
        return validUser(user);
    }

    public ArrayList<User> deleteUsers(ArrayList<Long> ids) {
        for (int i = 0; i < ids.size(); ++i) {
            deleteUser(ids.get(i));
        }
        return getUsersDetails();
    }

    public ArrayList<User> getUsersDetails() {
        ArrayList<User> usersDetails = new ArrayList<User>();

        Iterator<Long> it = users.keySet().iterator();
        while(it.hasNext()) {
            User u = users.get(it.next());
            usersDetails.add(u);
        }
        return usersDetails;
    }

    public User getUsers(Long id) {
        return users.get(id);
    }

    public User updateUsers(User user) {
        if(validUser(user)) {
            users.remove(user.getId());
            users.put(user.getId(), user);
        }
        return user;
    }
}
