package ru.jacobs.naumen.server.model;

import ru.jacobs.naumen.shared.User;

import java.util.ArrayList;

/**
 * Created by Jacob on 28.03.2016.
 */
public interface UserDAO {
    User addUsers(User user);
    Boolean deleteUser(Long id);
    Boolean testDuplicateUser(User user);
    ArrayList<User> getUsersDetails();
    User getUsers(Long id);
    User updateUsers(User user);
}
