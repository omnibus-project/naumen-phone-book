package ru.jacobs.naumen.client;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import net.customware.gwt.dispatch.client.DispatchAsync;
import ru.jacobs.naumen.event.*;
import ru.jacobs.naumen.presenter.EditUserPresenter;
import ru.jacobs.naumen.presenter.NotFoundPresenter;
import ru.jacobs.naumen.presenter.Presenter;
import ru.jacobs.naumen.presenter.UsersPresenter;
import ru.jacobs.naumen.view.EditUserView;
import ru.jacobs.naumen.view.NotFoundView;
import ru.jacobs.naumen.view.UsersView;

/**
 * Created by Jacob on 24.03.2016.
 */
public class AppController implements Presenter, ValueChangeHandler<String> {
    private final HandlerManager eventBus;
    private final DispatchAsync dispatch;
    private HasWidgets container;

    public AppController(DispatchAsync dispatch, HandlerManager eventBus) {
        this.eventBus = eventBus;
        this.dispatch = dispatch;
        bind();
    }

    private void bind() {
        History.addValueChangeHandler(this);
        eventBus.addHandler(AddUserEvent.TYPE,
                new AddUserEventHandler() {
                    public void onAddContact(AddUserEvent event) {
                        doAddNewUser();
                    }
                });
        eventBus.addHandler(EditUserEvent.TYPE,
                new EditUserEventHandler() {
                    public void onEditUser(EditUserEvent event) {
                        doEditUser(event.getId());
                    }
                });

        eventBus.addHandler(EditUserCancelledEvent.TYPE,
                new EditUserCancelledEventHandler() {
                    public void onEditContactCancelled(EditUserCancelledEvent event) {
                        doEditUserCancelled();
                    }
                });

        eventBus.addHandler(UpdateUserEvent.TYPE,
                new UpdateUserEventHandler() {
                    public void onUserUpdated(UpdateUserEvent event) {
                        doUserUpdated();
                    }
                });
    }

    private void doAddNewUser() {
        History.newItem("add");
    }

    private void doEditUser(long id) {
        History.newItem("edit", false);
        Presenter presenter = new EditUserPresenter(dispatch, eventBus, new EditUserView(), id);
        presenter.go(container);
    }

    private void doEditUserCancelled() {
        History.newItem("list");
    }

    private void doUserUpdated() {
        History.newItem("list");
    }

    public void go(final HasWidgets container) {
        this.container = container;

        if ("".equals(History.getToken())) {
            History.newItem("list");
        }
        else {
            History.fireCurrentHistoryState();
        }
    }

    public void onValueChange(ValueChangeEvent<String> event) {
        String token = event.getValue();

        Presenter presenter = null;
        if (token != null) {
            if (token.equals("list")) {
                presenter = new UsersPresenter(dispatch, eventBus, new UsersView());
            } else if (token.equals("add")) {
                presenter = new EditUserPresenter(dispatch, eventBus, new EditUserView());
            } else if (token.equals("edit")) {
                presenter = new EditUserPresenter(dispatch, eventBus, new EditUserView());
            } else {
                presenter = new NotFoundPresenter(eventBus, new NotFoundView());
            }

            if (presenter != null) {
                presenter.go(container);
            }
        }
    }
}
