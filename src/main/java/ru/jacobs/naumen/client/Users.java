package ru.jacobs.naumen.client;


import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import net.customware.gwt.dispatch.client.DefaultExceptionHandler;
import net.customware.gwt.dispatch.client.DispatchAsync;
import net.customware.gwt.dispatch.client.standard.StandardDispatchAsync;
import ru.jacobs.naumen.shared.User;
import ru.jacobs.naumen.shared.dispatch.GetUserListAction;
import ru.jacobs.naumen.shared.dispatch.GetUserListResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by Jacob on 23.03.2016.
 */
public class Users implements EntryPoint {
    public void onModuleLoad() {
        DispatchAsync dispatch = new StandardDispatchAsync(new DefaultExceptionHandler());
        HandlerManager eventBus = new HandlerManager(null);
        AppController appView = new AppController(dispatch, eventBus);
        appView.go(RootPanel.get());
    }
}
