package ru.jacobs.naumen.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by Jacob on 25.03.2016.
 */
public class EditUserEvent extends GwtEvent<EditUserEventHandler> {
    public static GwtEvent.Type<EditUserEventHandler> TYPE = new GwtEvent.Type<EditUserEventHandler>();
    private final long id;

    public EditUserEvent(long id) {
        this.id = id;
    }

    public long getId() { return id; }

    @Override
    public Type<EditUserEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EditUserEventHandler handler) {
        handler.onEditUser(this);
    }
}
