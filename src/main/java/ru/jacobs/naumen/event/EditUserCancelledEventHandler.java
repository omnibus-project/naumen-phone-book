package ru.jacobs.naumen.event;

import com.google.gwt.event.shared.EventHandler;
/**
 * Created by Jacob on 25.03.2016.
 */
public interface EditUserCancelledEventHandler extends EventHandler {
    void onEditContactCancelled(EditUserCancelledEvent event);
}
