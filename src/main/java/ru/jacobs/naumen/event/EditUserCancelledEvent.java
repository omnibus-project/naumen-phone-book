package ru.jacobs.naumen.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by Jacob on 25.03.2016.
 */
public class EditUserCancelledEvent  extends GwtEvent<EditUserCancelledEventHandler> {
    public static Type<EditUserCancelledEventHandler> TYPE = new Type<EditUserCancelledEventHandler>();

    @Override
    public Type<EditUserCancelledEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(EditUserCancelledEventHandler handler) {
        handler.onEditContactCancelled(this);
    }
}
