package ru.jacobs.naumen.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * Created by Jacob on 24.03.2016.
 */
public class AddUserEvent extends GwtEvent<AddUserEventHandler> {
    public static Type<AddUserEventHandler> TYPE = new Type<AddUserEventHandler>();

    @Override
    public Type<AddUserEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AddUserEventHandler handler) {
        handler.onAddContact(this);
    }
}
