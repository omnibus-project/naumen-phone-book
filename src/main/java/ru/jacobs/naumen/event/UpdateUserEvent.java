package ru.jacobs.naumen.event;

import com.google.gwt.event.shared.GwtEvent;
import ru.jacobs.naumen.shared.User;

/**
 * Created by Jacob on 25.03.2016.
 */
public class UpdateUserEvent extends GwtEvent<UpdateUserEventHandler> {
    public static Type<UpdateUserEventHandler> TYPE = new Type<UpdateUserEventHandler>();
    private final User updatedUser;


    public UpdateUserEvent(User updatedUser) {
        this.updatedUser = updatedUser;
    }

    public User getUpdatedUser() { return updatedUser; }

    @Override
    public Type<UpdateUserEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(UpdateUserEventHandler handler) {
        handler.onUserUpdated(this);
    }
}
