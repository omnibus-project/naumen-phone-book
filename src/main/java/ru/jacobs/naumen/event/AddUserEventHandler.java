package ru.jacobs.naumen.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Created by Jacob on 24.03.2016.
 */
public interface AddUserEventHandler extends EventHandler{
    void onAddContact(AddUserEvent event);
}
