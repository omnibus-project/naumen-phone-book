package ru.jacobs.naumen.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import ru.jacobs.naumen.presenter.NotFoundPresenter;
import com.google.gwt.user.client.ui.*;

/**
 * Created by Jacob on 28.03.2016.
 */
public class NotFoundView extends Composite implements NotFoundPresenter.Display {
    private final Panel panel;
    private final Button cancelButton;

    public NotFoundView() {
        panel = new FlowPanel();
        Label title = new Label();
        title.setText("Ошибка 404. Страница не найдена");
        panel.add(title);
        final Panel buttonsPanel = new FlowPanel();
        cancelButton = new Button("Вернуться на главную");
        buttonsPanel.add(cancelButton);
        panel.add(buttonsPanel);

        DecoratorPanel contentDetailsDecorator = new DecoratorPanel();
        contentDetailsDecorator.setWidget(panel);
        contentDetailsDecorator.getElement().setAttribute("align", "center");
        initWidget(contentDetailsDecorator);
    }

    @Override
    public HasClickHandlers getCancelClickHandlers() {
        return cancelButton;
    }

}
