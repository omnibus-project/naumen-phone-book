package ru.jacobs.naumen.view;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SingleSelectionModel;
import ru.jacobs.naumen.presenter.UsersPresenter;
import ru.jacobs.naumen.shared.User;

import java.util.List;

/**
 * Created by Jacob on 23.03.2016.
 */
public class UsersView extends Composite implements UsersPresenter.Display {
    private TextBox                             searchText;
    private Button                              searchButton;
    private Button                              addButton;
    private Button                              editDialogBoxButton;
    private Button                              deleteDialogBoxButton;
    private Button                              closeDialogBoxButton;
    private Button                              closeDeleteDialogBoxButton;
    private Button                              goodDeleteDialogBoxButton;
    private ButtonCell                          editButton;
    private ButtonCell                          deleteButton;
    private CellTable<User>                     table;
    private final ListDataProvider<User>        dataProvider;
    private final SingleSelectionModel<User>    selectionModel;
    private ClickableTextCell                   clickableTextCell;
    private Column                              editColumn;
    private Column                              deleteColumn;
    private Column                              nameColumn;
    private Column                              phoneColumn;
    private DialogBox                           dialogBox;

    public UsersView() {
            table            = new CellTable<User>();
            searchText       = new TextBox();
            searchButton     = new Button("Поиск");
            addButton        = new Button("Добавить контакт");
            editButton       = new ButtonCell();
            deleteButton     = new ButtonCell();
            clickableTextCell = new ClickableTextCell();


            searchText.getElement().setPropertyString("placeholder", "Введите имя или номер телефона...");

            table.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.ENABLED);

            // Create name column.
            nameColumn = new Column<User, String>(clickableTextCell) {
                @Override
                public String getValue(User object) {
                    return object.getName();
                }
            };

//          Создаем колонки для таблицы
            phoneColumn = new Column<User, String>(clickableTextCell) {
            @Override
            public String getValue(User object) {
                return object.getPhone();
            }
        };

            editColumn = new Column<User, String>(editButton) {
                @Override
                public String getValue(User object) {
                    return "Изменить";
                }
            };

            deleteColumn = new Column<User, String>(deleteButton) {
                @Override
                public String getValue(User object) {
                    return "Удалить";
                }
            };


//          Добавляем колонки в таблицу
            table.addColumn(nameColumn,     "Имя");
            table.addColumn(phoneColumn,    "Телефон");
            table.addColumn(editColumn,     "Изменить контакт");
            table.addColumn(deleteColumn,   "Удалить контакт");

            // Устанавливаем ширину
            table.setWidth("100%", true);

            // Уставливаем ширину каждого столбца
            table.setColumnWidth(nameColumn, 35.0, Style.Unit.PCT);
            table.setColumnWidth(phoneColumn, 35.0, Style.Unit.PCT);
            table.setColumnWidth(editColumn, 35.0, Style.Unit.PCT);
            table.setColumnWidth(deleteColumn, 35.0, Style.Unit.PCT);

            dataProvider = new ListDataProvider<User>();
            dataProvider.addDataDisplay(table);

            selectionModel = new SingleSelectionModel<User>();
            table.setSelectionModel(selectionModel);

//          Создаем панель и добавляем виджеты
            VerticalPanel panel = new VerticalPanel();
            HorizontalPanel hPanel = new HorizontalPanel();
            hPanel.setBorderWidth(0);
            hPanel.setSpacing(10);
            panel.getElement().setAttribute("align", "center");
            panel.setBorderWidth(1);
            panel.setWidth("800");
            hPanel.add(searchText);
            hPanel.add(searchButton);
            hPanel.add(addButton);
            panel.add(hPanel);
            panel.add(table);
            initWidget(panel);
        }

    @Override
    public HasClickHandlers getAddButton() {
        return addButton;
    }

    @Override
    public HasClickHandlers getSearchButton() {
        return searchButton;
    }

    @Override
    public HasClickHandlers getEditDialogBoxButton() {
        return editDialogBoxButton;
    }

    @Override
    public HasClickHandlers getDeleteDialogBoxButton() {
        return deleteDialogBoxButton;
    }

    @Override
    public HasClickHandlers getCloseDialogBoxButton() {
        return closeDialogBoxButton;
    }

    @Override
    public HasClickHandlers getCloseDeleteDialogBoxButton() {
        return closeDeleteDialogBoxButton;
    }

    @Override
    public HasClickHandlers getGoodDeleteDialogBoxButton() {
        return goodDeleteDialogBoxButton;
    }

    @Override
    public Column getDeleteButton() {
        return deleteColumn;
    }

    @Override
    public Column getEditButton() {
        return  editColumn;
    }


    @Override
    public Column getNameColumn() {
        return nameColumn;
    }

    @Override
    public Column getPhoneColumn() {
        return phoneColumn;
    }


    @Override
    public String getSearchText() {
        return searchText.getText();
    }

    @Override
    public void setData(List<User> data) {
        dataProvider.getList().clear();
        List<User> list = dataProvider.getList();
        for (User contact : data) {
            list.add(contact);
        }
        dataProvider.refresh();
    }

    @Override
    public DialogBox setDialogBox(long id, String name, String phone) {
        return createDialogBox(id, name, phone);
    }

    @Override
    public User getClickedRow() {
        return selectionModel.getSelectedObject();
    }


    @Override
    public void hideDialogBox() {
        dialogBox.hide();
    }

    @Override
    public void showDialogError(String errMsg) {
        createErrDialogBox(errMsg);
    }

    @Override
    public void showDialogDelete(String msg) {
        createDeleteDialogBox(msg);
    }

    private void createDeleteDialogBox(String msg) {
        dialogBox = new DialogBox();
        dialogBox.ensureDebugId("cwDialogBox");
//        dialogBox.setText("Ошибка!");

        VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setSpacing(4);
        dialogBox.setWidget(dialogContents);

        HTML idetails = new HTML(msg);
        dialogContents.add(idetails);
        dialogContents.setCellHorizontalAlignment(
                idetails, HasHorizontalAlignment.ALIGN_LEFT);

        closeDeleteDialogBoxButton = new Button("Отмена");
        goodDeleteDialogBoxButton = new Button("Да");

        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setSpacing(10);
        hPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        hPanel.add(closeDeleteDialogBoxButton);
        hPanel.add(goodDeleteDialogBoxButton);
        dialogContents.add(hPanel);

        if (LocaleInfo.getCurrentLocale().isRTL()) {
            dialogContents.setCellHorizontalAlignment(
                    closeDeleteDialogBoxButton, HasHorizontalAlignment.ALIGN_LEFT);
            dialogContents.setCellHorizontalAlignment(
                    goodDeleteDialogBoxButton, HasHorizontalAlignment.ALIGN_LEFT);

        } else {
            dialogContents.setCellHorizontalAlignment(
                    closeDeleteDialogBoxButton, HasHorizontalAlignment.ALIGN_RIGHT);
            dialogContents.setCellHorizontalAlignment(
                    goodDeleteDialogBoxButton, HasHorizontalAlignment.ALIGN_RIGHT);
        }

        dialogBox.setGlassEnabled(true);

        dialogBox.setAnimationEnabled(true);
        dialogBox.center();
        dialogBox.show();
    }

    private void createErrDialogBox(String errMsg) {
        dialogBox = new DialogBox();
        dialogBox.ensureDebugId("cwDialogBox");
        dialogBox.setText("Ошибка!");

        VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setSpacing(4);
        dialogBox.setWidget(dialogContents);

        HTML idetails = new HTML(errMsg);
        dialogContents.add(idetails);
        dialogContents.setCellHorizontalAlignment(
                idetails, HasHorizontalAlignment.ALIGN_LEFT);

        closeDialogBoxButton = new Button("Close");

        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setSpacing(10);
        hPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        hPanel.add(closeDialogBoxButton);
        dialogContents.add(hPanel);

        if (LocaleInfo.getCurrentLocale().isRTL()) {
            dialogContents.setCellHorizontalAlignment(
                    closeDialogBoxButton, HasHorizontalAlignment.ALIGN_LEFT);

        } else {
            dialogContents.setCellHorizontalAlignment(
                    closeDialogBoxButton, HasHorizontalAlignment.ALIGN_RIGHT);
        }

        dialogBox.setGlassEnabled(true);

        dialogBox.setAnimationEnabled(true);
        dialogBox.center();
        dialogBox.show();
    }

    private DialogBox createDialogBox(long id, String name, String phone) {
        dialogBox = new DialogBox();
        dialogBox.ensureDebugId("cwDialogBox");
        dialogBox.setText("Карточка контакта");

        VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setSpacing(4);
        dialogBox.setWidget(dialogContents);

        HTML idetails = new HTML("ID: " + id);
        dialogContents.add(idetails);
        dialogContents.setCellHorizontalAlignment(
                idetails, HasHorizontalAlignment.ALIGN_LEFT);

        HTML nameDetails = new HTML("Имя: " + name);
        dialogContents.add(nameDetails);
        dialogContents.setCellHorizontalAlignment(
                nameDetails, HasHorizontalAlignment.ALIGN_LEFT);

        HTML phoneDetails = new HTML("Номер телефона: " + phone);
        dialogContents.add(phoneDetails);
        dialogContents.setCellHorizontalAlignment(
                phoneDetails, HasHorizontalAlignment.ALIGN_LEFT);

        editDialogBoxButton = new Button("Изменить");
        deleteDialogBoxButton = new Button("Удалить");
        closeDialogBoxButton = new Button("Отменить");

        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setSpacing(10);
        hPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        hPanel.add(editDialogBoxButton);
        hPanel.add(deleteDialogBoxButton);
        hPanel.add(closeDialogBoxButton);
        dialogContents.add(hPanel);


        if (LocaleInfo.getCurrentLocale().isRTL()) {
            dialogContents.setCellHorizontalAlignment(
                    editDialogBoxButton, HasHorizontalAlignment.ALIGN_LEFT);
            dialogContents.setCellHorizontalAlignment(
                    deleteDialogBoxButton, HasHorizontalAlignment.ALIGN_LEFT);
            dialogContents.setCellHorizontalAlignment(
                    closeDialogBoxButton, HasHorizontalAlignment.ALIGN_LEFT);

        } else {
            dialogContents.setCellHorizontalAlignment(
                    editDialogBoxButton, HasHorizontalAlignment.ALIGN_RIGHT);
            dialogContents.setCellHorizontalAlignment(
                    deleteDialogBoxButton, HasHorizontalAlignment.ALIGN_RIGHT);
            dialogContents.setCellHorizontalAlignment(
                    closeDialogBoxButton, HasHorizontalAlignment.ALIGN_RIGHT);
        }

        dialogBox.setGlassEnabled(true);

        dialogBox.setAnimationEnabled(true);
        dialogBox.center();
        dialogBox.show();

        return dialogBox;
    }
}
