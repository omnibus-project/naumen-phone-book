package ru.jacobs.naumen.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.ui.*;
import ru.jacobs.naumen.presenter.EditUserPresenter;

/**
 * Created by Jacob on 24.03.2016.
 */
public class EditUserView extends Composite implements EditUserPresenter.Display {
        private final TextBox name;
        private final TextBox phone;
        private final Button saveButton;
        private final Button cancelButton;
        private Button closeDialogBoxButton;
        private DialogBox   dialogBox;

        public EditUserView() {
            FlexTable layout = new FlexTable();
            layout.setCellSpacing(6);
            FlexTable.FlexCellFormatter cellFormatter = layout.getFlexCellFormatter();

            layout.setHTML(0, 0, "Заполните поля контакта");
            cellFormatter.setColSpan(0, 0, 2);
            cellFormatter.setHorizontalAlignment(
                    0, 0, HasHorizontalAlignment.ALIGN_CENTER);

            name = new TextBox();
            phone = new TextBox();
            saveButton = new Button("Сохранить");
            cancelButton = new Button("Отмена");

            layout.setHTML(1, 0, "Имя контакта");
            layout.setWidget(1, 1, name);
            layout.setHTML(2, 0, "Номер телефона");
            layout.setWidget(2, 1, phone);
            layout.setWidget(3, 0, saveButton);
            layout.setWidget(3, 1,cancelButton);

            DecoratorPanel contentDetailsDecorator = new DecoratorPanel();
            contentDetailsDecorator.setWidget(layout);
            contentDetailsDecorator.getElement().setAttribute("align", "center");
            initWidget(contentDetailsDecorator);
        }

    @Override
    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    @Override
    public HasClickHandlers getCancelButton() {
        return cancelButton;
    }

    @Override
    public HasClickHandlers getCloseDialogBoxButton() {
        return closeDialogBoxButton;
    }

    @Override
    public HasValue<String> getName() {
        return name;
    }

    @Override
    public HasValue<String> getPhone() {
        return phone;
    }

    @Override
    public void showDialogError(String errMsg) {
        createErrDialogBox(errMsg);
    }

    @Override
    public void hideDialogError() {
        dialogBox.hide();
    }

    private void createErrDialogBox(String errMsg) {
        dialogBox = new DialogBox();
        dialogBox.ensureDebugId("cwDialogBox");
        dialogBox.setText("Ошибка!");

        VerticalPanel dialogContents = new VerticalPanel();
        dialogContents.setSpacing(4);
        dialogBox.setWidget(dialogContents);

        HTML idetails = new HTML(errMsg);
        dialogContents.add(idetails);
        dialogContents.setCellHorizontalAlignment(
                idetails, HasHorizontalAlignment.ALIGN_LEFT);

        closeDialogBoxButton = new Button("Close");

        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setSpacing(10);
        hPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
        hPanel.add(closeDialogBoxButton);
        dialogContents.add(hPanel);

        if (LocaleInfo.getCurrentLocale().isRTL()) {
            dialogContents.setCellHorizontalAlignment(
                    closeDialogBoxButton, HasHorizontalAlignment.ALIGN_LEFT);

        } else {
            dialogContents.setCellHorizontalAlignment(
                    closeDialogBoxButton, HasHorizontalAlignment.ALIGN_RIGHT);
        }

        dialogBox.setGlassEnabled(true);

        dialogBox.setAnimationEnabled(true);
        dialogBox.center();
        dialogBox.show();
    }

}
